(setq *print-case* :downcase)
#||
(defvar *age* 18)

(if(not(= *age* 18))
  (format t "You can vote ~%")
  (format t "You can't vote ~%"))

(format t "----------------~%")

(if (and (<= *age* 14)(>= *age* 67))
  (format t "Time for work ~%")
  (format t "Work if you want ~%"))


(if (or (<= *age* 14)(>= *age* 67))
  (format t "Time for work ~%")
  (format t "Work if you want ~%"))

;; THERE IS NO ELSE

(defvar *num* 2)
(defvar *num-2* 2)
(defvar *num-3* 2)

(if (= *num* 2)
  (progn
    (setf *num-2* (* *num-2* 2))
    (setf *num-3* (* *num-3* 3)))
  (format t "Not equal to 2 ~%"))

(format t "*num-2* = ~d ~%" *num-2*)
(format t "*num-3* = ~d ~%" *num-3*)
||#



;; CASE OPERATOR and CONDITIONAL

(defvar *age* 18)
(defvar *num-3* 5)
(defun get-school (age)
  (case age
    (5 (print "Kindergarden"))
    (6 (print "First grade"))
    (otherwise (print "Middle School"))))

(get-school *age*)
(terpri)

(when(= *age* 18)
  (setf *num-3* 18)
  (format t "Go to college, you're ~d ~%" *num-3*))

(unless (not (= *age* 18))
             (setf *num-3* 20)
             (format t "Something random ~%" *num-3*))

;; CONDITIONAL

(defvar *college-ready* nil)
(cond ( (>= *age* 18) 
       (setf *college-ready* 'yes)
       (format t "Ready to college ~%"))
      ( (< *age* 18)
            (setf *college-ready* 'no)
            (format t "Not ready for college ~%"))
      (t (format t "Dont't Know ~%"))) ; same as else, t == true ... "If you got here, then..."
