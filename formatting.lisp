;;(defvar *number* 0)
;;(setf *numer* 6)

(format t "Number with commas ~:d ~%" 1000000)
(format t "PI with 4 characters ~,4f ~%" 3.141593)
(format t "10 Percnt ~,,2f ~%" .10)
(format t "10 Dollars ~$ ~%" 10)

#||
Quasi Quoting:
-> Switches from code to Data Mode
||#

#||
(defparameter *human-bullshit-irrational*
  '((Religious (+10x1000^999 Blevel) (-1000^-999^...n Ilevel))
    (Altruim (+10x1000^7 Blevel) (-100^9999 Ilevel))
    (Humans (0897586 Blevel) (098.6.890))))

(defun get-bullshit (size)
  (format t "::: ~a~%"
    `(, (caar size) is , (cadar size) and ,(cddar size)))) ; QuaseData |` |

(get-bullshit *human-bullshit-irrational*)



||#

