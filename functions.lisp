(setq *print-case* :downcase)
#||
;;; Defining a function
(defun hello()
  (print "Hello")
  (terpri))

(hello)

;;;-----------------------------------------

(defun get_agv (num num0)
  (/ (+ num num0) 2))

(format t "Aerage 200 and 50 = ~a~%"(get_agv 200 50))

;;;---------------------------------------------------

;;; Optional arguments
(defun print_list (w x &optional y z)
  (format t "List = ~a~%" (list w x y z)))

(print_list 1 2 3)

;;;---------------------------------------------

(defvar *total* 0)
(defun sum (&rest nums)
  (dolist (num nums)
    (setf *total* (+ *total* num)))
  (format t "Sum = ~a~%" *total*))

(sum 1 2 3 4 5 6 7)


;;;------------------------------------------------


(defun print-list(&optional &key x y z)
  (format t "List: ~a~%" (list x y z)))

(print-list :x 1 :y 2 :z "A")


;;; Remember to put return-from to return a value

(defun diff (num num0)
  (return-from diff(- num num0))) ; like in here

(format t "Diff 500 97 => [~a]~%"(diff 500 97))

;;;------------------------------------------------------


;;; example mapcar usage (takes a list and runs a function over and over
;;; in this case it tests whether it's a number or not

(format t "A number ~a~%" (mapcar #'numberp '(1 2 3 f g)))

;;;----------------------------------------------------------------



;;; Local only functions

;;; ex
(flet ((func_name (arguments)
    (function Body ))
    body))


(flet ((double-it (num)
                  (* num 2))
       (triple-it (num)
                  (* num 3)))
  (format t "Double and Triple of 10: [~a]~%" (triple-it(double-it 10))))

;--------------------------------------------------------------------


;;; labels

(labels ((double-it (num)
                   (* num 2))
        (triple-it (num)
                   (* (double-it num) 3)))
  (format t "Double and Triple 2 = ~d~%" (triple-it 3)))

;--------------------------------------------------------------------

;;; return multiple values

(defun squares (num)
  (values (expt num 2) (expt num 3)))

(multiple-value-bind (a b) (squares 2)
  (format t "2^2 = ~d | 2^3 = ~d~%" a b))

(multiple-value-bind 2 5)
 ||#

;;------------------------------------------------------------------

;;; Higher order functions







