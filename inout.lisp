;;;; Beginning of the code
;;; Beginning of the line
    ;; indented comment along the code block

;;; (format t "hello, world ~%")   ; comment indented beside the code block

;;; to comment out just a specifit piece of code: 
;;; add:  #+(or), change or-> and, it'll print out the message

#+(or)(print "I'm not being printed")
#+(and)(print "I'm being printed")
#||
(Print "Enter your name: ")
(defvar *name* (read))
(defun hello_you (*name*)
  (format t "Hello ~a! ~%" *name*))

(setq *print-case* :downcase)
(hello_you *name*)
||#
