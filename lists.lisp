(setq *print-case* :downcase)
#||
(cons 'superman 'batman)                            
(list 'superman 'batman 'flash)
(cons 'aquaman '(superman batman))
||#

#||
;;; getting items from the list

(format t "First = ~a ~%" (car '(superman batman aquaman)))

(format t "Except Superman = ~a ~%" (cdr '(superman batman aquaman)))

(format t "Second [2nd] = ~a ~%" (cadr '(superman batman aquaman flash joker)))

(format t "Third [3rd] = ~a ~%" (caddr '(superman batman aquaman flash joker)))

(format t "Forth [4th] = ~a ~%" (cadddr '(superman batman aquaman flash joker)))


;;; List checking: 

(format t "Is it a list = ~a ~%" (listp '(batman superman)))
(format t "Is 3 i in list = ~a ~%" (if (member 3 '(2 4 6))'t nil))

(append '(just) '(some) '(random words))

(defparameter *nums* '(2 4 6))
(push 1 *nums*)
(format t "2nd Item in list is: ~a ~%" (nth 2 *nums*))


;;; like for k, v in dict.items() 
(defvar superman (list :name "Superman" :secret-id "Clark Kent"))
(defvar *hero-list* nil)
(push superman *hero-list*) ; add superman to hero-list
(dolist (hero *hero-list*)
  (format t "~{|~a : ~a ~}~%" hero))

||#

#||
;;; Association list

(defparameter *heroes*
  '((Superman (Clark Kent))
    (Flash (Barry Allen))
    (Batman (Bruce Wayne))))
(format t "Superman Data ~a ~%" (assoc 'superman *heroes*))
(format t "Superman is ~a ~%" (cadr (assoc 'superman *heroes*)))

||#

(defparameter *hero-size*
  '((Superman (1 m 80 cm) (180 kg))
    (Flash (1 m 77 cm) (80 kg))
    (Batman (1 m 90 cm) (160 kg))))

(format t "Flash is ~a tall ~%" (cadr (assoc 'Flash *hero-size*)))
(format t "Flash weights ~a ~%" (caddr (assoc 'Flash *hero-size*)))
